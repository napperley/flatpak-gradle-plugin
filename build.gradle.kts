group = "org.digieng"
version = "0.1-SNAPSHOT"

plugins {
    `kotlin-dsl`
    id("java-gradle-plugin")
}

repositories {
    jcenter()
}

dependencies {
    val koTestVer = "4.3.2"
    testImplementation("io.kotest:kotest-runner-junit5:$koTestVer")
    testImplementation("io.kotest:kotest-assertions-core:$koTestVer")
}

gradlePlugin {
    plugins {
        create("flatpak") {
            id = "org.digieng.flatpak"
            implementationClass = "org.digieng.flatpakPlugin.FlatpakPlugin"
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

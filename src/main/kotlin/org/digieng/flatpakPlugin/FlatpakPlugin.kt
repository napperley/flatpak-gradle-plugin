package org.digieng.flatpakPlugin

import org.digieng.flatpakPlugin.task.FlatpakManifestTask
import org.gradle.api.Plugin
import org.gradle.api.Project

open class FlatpakPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.tasks.create("flatpakManifest", FlatpakManifestTask::class.java)
    }
}

package org.digieng.flatpakPlugin.buildSystem

class SimpleBuildSystem : BuildSystem {
    override val name: String = "simple"
    val buildCommands: MutableList<String> = mutableListOf()
}

fun simpleBuildSystem(init: SimpleBuildSystem.() -> Unit): SimpleBuildSystem {
    val buildSystem = SimpleBuildSystem()
    buildSystem.init()
    return buildSystem
}

package org.digieng.flatpakPlugin.module

import org.digieng.flatpakPlugin.source.Source

interface Module {
    var name: String
    val sources: MutableList<Source>
}

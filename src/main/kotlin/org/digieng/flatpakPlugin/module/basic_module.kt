package org.digieng.flatpakPlugin.module

import org.digieng.flatpakPlugin.buildSystem.BuildSystem
import org.digieng.flatpakPlugin.source.Source

class BasicModule : Module {
    override var name: String = ""
    override val sources: MutableList<Source> = mutableListOf()
    lateinit var buildSystem: BuildSystem
}

fun basicModule(init: BasicModule.() -> Unit): BasicModule {
    val module = BasicModule()
    module.init()
    return module
}

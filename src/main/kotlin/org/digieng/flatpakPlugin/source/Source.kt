package org.digieng.flatpakPlugin.source

interface Source {
    val type: String
}

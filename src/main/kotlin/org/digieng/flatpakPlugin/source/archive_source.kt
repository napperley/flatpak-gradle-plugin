package org.digieng.flatpakPlugin.source

import java.net.URL

class ArchiveSource : Source {
    override val type: String = "archive"
    lateinit var url: URL
    var sha256: String = ""
    var sha512: String = ""
}

fun archiveSource(init: ArchiveSource.() -> Unit): ArchiveSource {
    val source = ArchiveSource()
    source.init()
    return source
}

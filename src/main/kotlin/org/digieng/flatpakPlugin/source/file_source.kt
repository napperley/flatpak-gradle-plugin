package org.digieng.flatpakPlugin.source

class FileSource : Source {
    override val type: String = "file"
    var path: String = ""
}

fun fileSource(init: FileSource.() -> Unit = {}): FileSource {
    val source = FileSource()
    source.init()
    return source
}

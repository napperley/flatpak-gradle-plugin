package org.digieng.flatpakPlugin.task

import org.digieng.flatpakPlugin.module.Module
import org.gradle.api.DefaultTask
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.property

open class FlatpakManifestTask : FlatpakManifestTaskBase, DefaultTask() {
    private val objFactory = project.objects
    @Input
    override val appId: Property<String> = objFactory.property()
    @Input
    override val command: Property<String> = objFactory.property()
    @Input
    override val runtime: Property<String> = objFactory.property()
    @Input
    override val runtimeVer: Property<String> = objFactory.property()
    @Input
    override val sdk: Property<String> = objFactory.property()
    @Input
    override val modules: ListProperty<Module> = objFactory.listProperty(Module::class.java)
    @Input
    override val renameIcon: Property<String> = objFactory.property()
    @Input
    override val renameDesktopFile: Property<String> = objFactory.property()
    @Input
    override val finishArgs: ListProperty<String> = objFactory.listProperty(String::class.java)
    @Input
    override val cleanup: ListProperty<String> = objFactory.listProperty(String::class.java)
    @Input
    override val cleanupCommands: ListProperty<String> = objFactory.listProperty(String::class.java)

    init {
        @Suppress("LeakingThis")
        renameDesktopFile.set("")
        @Suppress("LeakingThis")
        renameIcon.set("")
    }

    @TaskAction
    fun create() {
        // TODO: Create the Flatpak.
        println(
            """
            Creating a Flatpak with a manifest that contains the following:
              app-id: ${appId.get()}
              command: ${command.get()}
              runtime: ${runtime.get()}
              runtime-version: ${runtimeVer.get()}
              sdk: ${sdk.get()}
              rename-desktop-file: ${renameDesktopFile.get()}
              rename-icon: ${renameIcon.get()}
            """.trimIndent()
        )
    }
}

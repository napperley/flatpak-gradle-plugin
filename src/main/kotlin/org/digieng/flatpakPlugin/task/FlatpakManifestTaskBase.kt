package org.digieng.flatpakPlugin.task

import org.digieng.flatpakPlugin.module.Module
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property

interface FlatpakManifestTaskBase {
    val appId: Property<String>
    val command: Property<String>
    val runtime: Property<String>
    val runtimeVer: Property<String>
    val sdk: Property<String>
    val modules: ListProperty<Module>
    val renameIcon: Property<String>
    val renameDesktopFile: Property<String>
    val finishArgs: ListProperty<String>
    val cleanup: ListProperty<String>
    val cleanupCommands: ListProperty<String>
}

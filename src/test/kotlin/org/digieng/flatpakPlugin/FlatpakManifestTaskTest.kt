package org.digieng.flatpakPlugin

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.string.shouldContain
import org.gradle.testkit.runner.GradleRunner
import java.io.File

class FlatpakManifestTaskTest : FunSpec() {
    private val pluginBlock = """
        plugins {
            id("org.digieng.flatpak")
        }
    """.trimIndent()
    private val projectDir = File("/tmp/temp_project")
    private val buildFile = File("$projectDir/build.gradle")

    init {
        testTaskExists()
    }

    private fun setup() {
        projectDir.mkdir()
        buildFile.createNewFile()
    }

    private fun tearDown() {
        projectDir.deleteRecursively()
    }

    private fun testTaskExists() = test("Task exists") {
        setup()
        buildFile.writeText("""
            $pluginBlock
            
            flatpakManifest {
                appId = "org.example.Hello"
                command = "hello"
                runtime = "org.freedesktop.Platform"
                runtimeVer = "19.08"
                sdk = "org.freedesktop.Sdk"
            }
        """.trimIndent())
        val result = GradleRunner
            .create()
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .withArguments("flatpakManifest")
            .build()
        result.output shouldContain "BUILD SUCCESSFUL"
        tearDown()
    }
}

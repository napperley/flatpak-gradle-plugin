package org.digieng.flatpakPlugin

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import org.gradle.testkit.runner.GradleRunner
import java.io.File

class FlatpakPluginTest : FunSpec() {
    private val projectDir = File("/tmp/temp_project")
    private val buildFile = File("$projectDir/build.gradle")

    init {
        testPluginApplied()
    }

    private fun setup() {
        projectDir.mkdir()
        buildFile.createNewFile()
        buildFile.writeText("""
            plugins {
                id("org.digieng.flatpak")
            }
        """.trimIndent())
    }

    private fun tearDown() {
        projectDir.deleteRecursively()
    }

    private fun testPluginApplied() = test("Plugin is applied") {
        val errorMsg = "Plugin [id: 'org.digieng.flatpakx'] was not found in any of the following sources"
        setup()
        val result = GradleRunner
            .create()
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .build()
        (errorMsg !in result.output) shouldBe true
//        result.task(":flatpak")?.outcome shouldBe true
        tearDown()
    }
}
